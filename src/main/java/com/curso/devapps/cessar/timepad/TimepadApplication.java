package com.curso.devapps.cessar.timepad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TimepadApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimepadApplication.class, args);
	}

}
