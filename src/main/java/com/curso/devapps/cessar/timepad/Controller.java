package com.curso.devapps.cessar.timepad;

import com.curso.devapps.cessar.timepad.model.Task;
import com.curso.devapps.cessar.timepad.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/timepad")
public class Controller {

    private TaskRepository repository;

    public Controller(@Autowired TaskRepository repository) {
        this.repository = repository;
    }

    @PostMapping(value = "task")
    public Task insertTask(@RequestBody Task task) {
        return repository.save(task);
    }

    @GetMapping(value = "task")
    public List<Task> getTasks() {
        return repository.findAll();
    }

    @GetMapping(value = "task/{id}")
    @ResponseBody
    public Task getTaskById(@PathVariable Integer id) {
        return repository.findById(id).get();
    }

    @PutMapping(value = "task")
    private Task updateTask(@RequestBody Task task) {
        return repository.save(task);
    }

    @DeleteMapping(value = "task")
    public Boolean deleteTask(@RequestBody Task task) {
        try {
            repository.delete(task);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
