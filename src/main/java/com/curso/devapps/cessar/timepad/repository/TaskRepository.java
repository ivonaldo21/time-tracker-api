package com.curso.devapps.cessar.timepad.repository;

import com.curso.devapps.cessar.timepad.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Integer> {
}
